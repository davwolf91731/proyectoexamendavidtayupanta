//
//  LibrosPorTitulo.swift
//  ExamenTayupantaDavid
//
//  Created by david tayupanta on 13/12/17.
//  Copyright © 2017 david tayupanta. All rights reserved.
//

import Foundation
import ObjectMapper
class LibrosPorTitulo:Mappable{
    
    var lbTitulo:String?
    var lbAuthor:String?
    var lbFecha:String?
    var lbDescripcion:String?
    
    required init?(map: Map){
        
    }
    
    func mapping(map: Map){
        lbTitulo <- map["items.0.volumeInfo.title"]
        lbAuthor <- map["items.0.volumeInfo.authors.0"]
        lbFecha <- map["items.0.volumeInfo.publishedDate.0"]
        lbDescripcion <- map["items.0.volumeInfo.description.0"]
        
    }
    
}
